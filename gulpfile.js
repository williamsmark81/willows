var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var nano = require('gulp-cssnano');
var htmlmin = require('gulp-htmlmin');
var imagemin = require('gulp-imagemin');

gulp.task('images', () =>
    gulp.src('app/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/images'))
);

// Check files to minify
gulp.task('useref', function(){
  return gulp.src('app/*.html')
    .pipe(useref())
    // Minifies JS files
    // .pipe(gulpIf('*.js', uglify()))
    // Minifies HTML files
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(htmlmin({removeComments: true}))
    // Minify CSS files
    .pipe(gulpIf('*.css', nano()))
    .pipe(gulp.dest('dist'))
});

// Copy fonts from app to dist
gulp.task('fonts', function(){
  return gulp.src('app/fonts/*')
  .pipe(gulp.dest('dist/fonts'))
});

// Copy fonts from app to dist
gulp.task('img', function(){
  return gulp.src('app/images/*')
  .pipe(gulp.dest('dist/images'))
});


// Watch files for changes and reload browser
gulp.task('watch', ['browserSync', 'pro-watch'], function(){
  // watch files for changes
  gulp.watch('app/scss/*.scss', ['pro-watch']); //SASS file save call pro-watch function
  gulp.watch('app/*.html', browserSync.reload); // HTML save, reload
  gulp.watch('app/js/*.js', browserSync.reload); // JS save, reload
});

// Create live server to view site
gulp.task('browserSync', function(){
  browserSync.init({
    server: {
      baseDir: 'app'
    },
  })
});

// SASS to CSS
gulp.task('pro-watch', function(){
  // compile sass to css
  return gulp.src('app/scss/*.scss')
  .pipe(sass()) // Convert SASS to CSS
  .pipe(gulp.dest('app/css')) // Save CSS in dist/css folder
  .pipe(browserSync.reload({ // Reload browserSync when file saved
    stream: true
  }))
});
